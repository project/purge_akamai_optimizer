<?php

/**
 * @file
 * Hooks related to akamai cache tag generation and purging.
 */

/**
 * Provides a mechanism to define and alter the site identifier tag.
 *
 * This is added on all the HTML pages using event subscribers and then it is
 * used for purging whole html at akamai during everything purge.
 *
 * @param string $site_identifier_tag
 *   The current site identifier.
 *
 * @see _akamai_site_identifier_tag()
 */
function hook_akamai_site_identifier_alter(string &$site_identifier_tag) {
  // Use business logic to figure out the unique identifier for the site and
  // then assign it to site_identifier_tag variable.
  $site_identifier_tag = 'new_site_identifier';
}

/**
 * Provides a mechanism to define and alter the file identifier tag.
 *
 * This is used for initiating file purging at akamai.
 * By default this module do not add it on file responses in edge cache tag
 * header and it has to be added on those paths via other mechanisms like
 * htaccess or changing Akamai CDN configurations.
 *
 * @param string $file_identifier_tag
 *   The file identifier tag.
 *
 * @see _akamai_file_identifier_tag()
 */
function hook_akamai_file_identifier_alter(string &$file_identifier_tag) {
  // Use business logic to figure out the unique identifier for the files and
  // then assign it to file_identifier_tag variable.
  $file_identifier_tag = 'new_file_identifier';
}

/**
 * Provides a mechanism to alter the prefix hash for cache tags.
 *
 * This is used for calculating unique hashes across a multisite setup.
 * By default it points to site path in a multisite setup.
 *
 * @param string $identifier
 *   The identifier prefix for hashes.
 *
 * @see \Drupal\purge_akamai_optimizer\EventSubscriber\TagsHashingSubscriber::__construct()
 */
function hook_akamai_hash_prefix_alter(string &$identifier) {
  // Use business logic to figure out the unique identifier for the hash prefix.
  $identifier = 'new_identifier';
}
