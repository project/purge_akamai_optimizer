<?php

/**
 * @file
 * Includes customization for akamai CDN caching.
 */

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\purge_akamai_optimizer\EventSubscriber\TagsHashingSubscriber;

/**
 * Implements hook_help().
 */
function purge_akamai_optimizer_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    // Main module help for the purge_akamai_optimizer module.
    case 'help.page.purge_akamai_optimizer':
      $output = '<pre>' . file_get_contents(\Drupal::service('extension.list.module')->getPath('purge_akamai_optimizer') . '/README.md') . '</pre>';
      return $output;

    default:
  }
}

/**
 * Implements hook_cron().
 */
function purge_akamai_optimizer_cron() {
  $tags_reducer = Drupal::service('purge_akamai_optimizer.reduce_tags');
  $tags_reducer->removeOldIdentifiers();

  if ($tags_reducer->shouldGeneratePriorityTags()) {
    $tags_reducer->generatePriorityListTags();
  }
}

/**
 * Helper function to return the site identifier cache tag.
 *
 * @return string
 *   Tag representing the string name of the site.
 */
function _akamai_site_identifier_tag() {
  $site_identifier_tag = '';

  // Allow other modules to define / alter site identifier.
  \Drupal::moduleHandler()->alter('akamai_site_identifier', $site_identifier_tag);

  // Since site identifier is added on the page, Prepend it with identifier
  // prefix.
  if ($site_identifier_tag) {
    $site_identifier_tag = TagsHashingSubscriber::IDENTIFIER_PREFIX . '_' . $site_identifier_tag;
  }

  return $site_identifier_tag;
}

/**
 * Helper function to return the file identifier cache tag.
 *
 * @return string
 *   Tag representing the string name of the file identifier.
 */
function _akamai_file_identifier_tag() {
  $file_identifier_tag = '';

  // Allow other modules to define / alter file identifier.
  \Drupal::moduleHandler()->alter('akamai_file_identifier', $file_identifier_tag);

  return $file_identifier_tag;
}
