<?php

namespace Drupal\purge_akamai_optimizer\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Drupal\akamai\Event\AkamaiPurgeEvents;
use Drupal\akamai\Event\AkamaiHeaderEvents;
use Drupal\purge_akamai_optimizer\Services\ReduceTags;
use Drupal\Component\Utility\Crypt;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\purge_akamai_optimizer\Services\QueueTags;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\purge_akamai_optimizer\Form\PurgeAkamaiOptimizerSettings;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * A Class for TagsHashingSubscriber.
 *
 * Applies hashing for Drupal cache tags send to Akamai.
 */
class TagsHashingSubscriber implements EventSubscriberInterface {

  /**
   * A unique prefix for the identifier tags.
   */
  const IDENTIFIER_PREFIX = 'pao';

  /**
   * The Drupal site identifier.
   *
   * @var string
   */
  protected $siteIdentifier = '';

  /**
   * The ReduceTags service.
   *
   * @var Drupal\purge_akamai_optimizer\Services\ReduceTags
   */
  protected $reduceTags;

  /**
   * The Current user.
   *
   * @var Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The Queue tags service.
   *
   * @var Drupal\purge_akamai_optimizer\Services\QueueTags
   */
  protected $queueTags;

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Logger service.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * Constructs a TagsHashingSubscriber object.
   *
   * @param string $site_path
   *   The site path.
   * @param Drupal\purge_akamai_optimizer\Services\ReduceTags $reduce_tags
   *   The site path.
   * @param Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user.
   * @param Drupal\purge_akamai_optimizer\Services\QueueTags $queue_tags
   *   The queue tags service.
   * @param Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory object.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(string $site_path,
    ReduceTags $reduce_tags,
    AccountProxyInterface $current_user,
    QueueTags $queue_tags,
    ConfigFactoryInterface $config_factory,
    LoggerInterface $logger,
    ModuleHandlerInterface $module_handler) {

    // Store the module_handler.
    $this->moduleHandler = $module_handler;

    // Get the Drupal sitepath from service, allow other modules to alter it and
    // use it as identifier.
    $identifier = $site_path;
    $this->moduleHandler->alter('akamai_hash_prefix', $identifier);
    $this->siteIdentifier = $identifier;

    // Stores an instance of reduce tags service.
    $this->reduceTags = $reduce_tags;

    // Current user object.
    $this->currentUser = $current_user;

    // Tags queuer service.
    $this->queueTags = $queue_tags;

    // The config factory object.
    $this->configFactory = $config_factory;

    // The logger service.
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    $events[AkamaiPurgeEvents::PURGE_CREATION][] = ['onPurgeCreation'];
    $events[AkamaiHeaderEvents::HEADER_CREATION][] = ['onHeaderCreation'];

    // Respond after FinishResponseSubscriber by setting low priority.
    $events[KernelEvents::RESPONSE][] = ['edgeControlHeader', -1025];
    return $events;
  }

  /**
   * Instucts akamai cdn to disable its caching.
   *
   * @param \Symfony\Component\EventDispatcher\Event $event
   *   The dispatched event.
   */
  public function edgeControlHeader(ResponseEvent $event) {
    if (!$event->isMasterRequest()) {
      return;
    }

    $response = $event->getResponse();

    $value = $this->configFactory->getEditable(PurgeAkamaiOptimizerSettings::SETTINGS)->get('disable_akamai_caching');

    if ($value) {
      $response->headers->set('Edge-Control', 'no-cache, no-store');
    }
  }

  /**
   * Applies hashing for Drupal cache tags send to Akamai for purging.
   *
   * @param Drupal\akamai\Event\AkamaiPurgeEvents $event
   *   The event to process.
   */
  public function onPurgeCreation(AkamaiPurgeEvents $event) {
    $tags = $event->data;
    $hashed_tags = [];

    $custom_akamai_settings = $this->configFactory->getEditable(PurgeAkamaiOptimizerSettings::SETTINGS);

    // In debugging mode if disable hashing is enabled then return.
    if ($custom_akamai_settings->get('disable_hashing')) {
      return;
    }
    $start_time = microtime(TRUE);

    // Get all the identifier tags and seperate out to ensure that they are not
    // rehashed again.
    $identifier_tags = [];
    $non_identifier_tags = [];
    foreach ($tags as $tag) {
      if (substr($tag, 0, strlen(self::IDENTIFIER_PREFIX)) === self::IDENTIFIER_PREFIX || $tag == _akamai_file_identifier_tag()) {
        $identifier_tags[] = $tag;
      }
      else {
        $non_identifier_tags[] = $tag;
      }
    }

    if (!empty($non_identifier_tags)) {
      $hashed_tags = $this->cacheTags($non_identifier_tags);

      // Passing 0 will return all the tags which can be removed.
      $reduced_tags = $this->reduceTags->removeExtraTags($non_identifier_tags, 0);

      if (!empty($reduced_tags['tags_removed'])) {
        // Get the list of replacement identifiers and add them for purging.
        $limit = ReduceTags::MAX_AKAMAI_TAGS - count($tags);
        $identifiers = $this->getIdentifiers($reduced_tags['tags_removed'], $limit, $custom_akamai_settings->get('replace_threshold'));
        $hashed_tags = array_merge($hashed_tags, $identifiers);
      }
    }

    if (isset($hashed_tags['site_identifier_tag_added'])) {
      // We have added site identifier tag, hence ignore everything else.
      unset($hashed_tags['site_identifier_tag_added']);
      $event->data = [end($hashed_tags)];
    }
    else {
      // Generate the final unique tags by combining everything.
      $event->data = array_unique(array_merge($identifier_tags, $hashed_tags));
    }
    $end_time = microtime(TRUE);
    $this->logPerformance((int) $start_time, (int) $end_time, __METHOD__);
  }

  /**
   * Applies hashing for cache tags inside edge cache tag header.
   *
   * @param Drupal\akamai\Event\AkamaiHeaderEvents $event
   *   The event to process.
   */
  public function onHeaderCreation(AkamaiHeaderEvents $event) {
    $tags = $event->data;
    $custom_akamai_settings = $this->configFactory->getEditable(PurgeAkamaiOptimizerSettings::SETTINGS);

    // In debugging mode if disable hashing is enabled then return.
    if ($custom_akamai_settings->get('disable_hashing')) {
      return;
    }
    $start_time = microtime(TRUE);

    if ($this->currentUser->isAnonymous()) {
      // Since akamai CDN applies for anonymous users only, hence it is best to
      // reduce the tags for anonymous users only.
      $reduced_tags = $this->reduceTags->removeExtraTags($tags);
      $event->data = $this->cacheTags($reduced_tags['tags_keep']);

      // Store the removed tags in db and get an identifier.
      if (!empty($reduced_tags['tags_removed'])) {
        $event->data[] = $this->storeRemovedTags($reduced_tags['tags_removed']);
      }
    }
    else {
      // For authenticated users we still need to apply hashing to prevent
      // header sizes going above the server limits.
      $event->data = $this->cacheTags($tags);
    }

    // Add the site identifier tag in the list of cachetags.
    $site_identifier_tag = _akamai_site_identifier_tag();
    if ($site_identifier_tag) {
      $event->data[] = $site_identifier_tag;
    }

    $end_time = microtime(TRUE);
    $this->logPerformance((int) $start_time, (int) $end_time, __METHOD__);
  }

  /**
   * Log performance time.
   *
   * @param int $start_time
   *   The start time.
   * @param int $end_time
   *   The end time.
   * @param string $method
   *   The name of the calling method.
   */
  protected function logPerformance(int $start_time, int $end_time, $method) {
    $custom_akamai_settings = $this->configFactory->getEditable(PurgeAkamaiOptimizerSettings::SETTINGS);
    $monitor_performance = $custom_akamai_settings->get('monitor_performance');
    if ($monitor_performance) {
      $this->logger->notice('%method Start: %start_time, End: %end_time, Differ: %differ',
        [
          '%method' => $method,
          '%start_time' => $start_time,
          '%end_time' => $end_time,
          '%differ' => $end_time - $start_time,
        ]);
    }
  }

  /**
   * Store the tags in db and return the identifier.
   *
   * @param array $tags_removed
   *   The removed tags.
   *
   * @return string
   *   The unique identifier
   */
  public function storeRemovedTags(array $tags_removed) {
    // Add a unique prefix to the identifier along with 10 character hash.
    $identifier = self::IDENTIFIER_PREFIX . '_' . $this->hashGenerator(implode(ReduceTags::DELIMITER, $tags_removed), 10);
    $this->reduceTags->storeRemovedTags($identifier, $tags_removed);
    return $identifier;
  }

  /**
   * Get the identifiers for all the tags stored in the db.
   *
   * @param array $tags
   *   Tags for which identifers are needed.
   * @param int $limit
   *   Maximum number of identifiers to return.
   * @param int $replace_threshold
   *   Maximum number of identifiers which will be queued.
   *
   * @return array
   *   The list of identifiers.
   */
  public function getIdentifiers(array $tags, $limit = 0, $replace_threshold = 500) {
    $all_identifiers = $this->reduceTags->getIdentifiers($tags);

    if (empty($all_identifiers)) {
      return [];
    }

    $identifier_count = count($all_identifiers);
    if ($identifier_count >= $replace_threshold) {
      $site_identifier_tag = _akamai_site_identifier_tag();
      if ($site_identifier_tag) {
        $this->logger->notice('Replacement threshold is set to %replace_threshold and generated replacements are %identifier_count hence using site identifier tag instead for purging akamai.',
          [
            '%identifier_count' => $identifier_count,
            '%replace_threshold' => $replace_threshold,
          ]);
        return [$site_identifier_tag, 'site_identifier_tag_added' => TRUE];
      }
    }

    // This will hold all the identifiers which will be purged now.
    $purge_identifiers = [];

    // This will hold all the identifers which will be queued for purging later.
    $queue_identifiers = [];

    // Generate purge_identifiers.
    $chunks = array_chunk($all_identifiers, $limit);
    $purge_identifiers = $chunks[0];
    unset($chunks[0]);

    // Generate queue identifiers.
    if (!empty($chunks)) {
      foreach ($chunks as $chunk) {
        $queue_identifiers = array_merge($queue_identifiers, $chunk);
      }
      // Add the remaining tags in the purge queue.
      $this->queueTags->queueTags($queue_identifiers);
    }
    return $purge_identifiers;
  }

  /**
   * Create a hash with the given input and length.
   *
   * @param string $input
   *   The input string to be hashed.
   * @param int $length
   *   The length of the hash.
   *
   * @return string
   *   Cryptographic hash with the given length.
   */
  public function hashGenerator(string $input, int $length) {
    // Generate hash with hashBase64 as it is more secure and less collision
    // risk.
    $hex = Crypt::hashBase64($input);

    // Remove all special characters.
    $hash = preg_replace("/[^A-Za-z0-9]/", '', $hex);

    // Return a hash with consistent length, padding zeroes if needed.
    return strtolower(str_pad(substr($hash, 0, $length), $length, '0', STR_PAD_LEFT));
  }

  /**
   * Create unique hashes/IDs for a list of cache tag strings.
   *
   * @param string[] $tags
   *   Non-associative array cache tags.
   *
   * @return string[]
   *   Non-associative array with hashed copies of the given cache tags.
   */
  public function cacheTags(array $tags) {
    $hashes = [];
    foreach ($tags as $tag) {
      // Prepend the tag with site identifier.
      $site_identifier_tag = $this->siteIdentifier . ':' . $tag;

      // Create an hash from this updated tag.
      $hashes[] = $this->hashGenerator($site_identifier_tag, 5);
    }
    return $hashes;
  }

}
