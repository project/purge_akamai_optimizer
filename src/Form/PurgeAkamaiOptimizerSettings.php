<?php

namespace Drupal\purge_akamai_optimizer\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactoryInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\purge_akamai_optimizer\Services\ReduceTags;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * PurgeAkamaiOptimizerSettings for this site.
 */
class PurgeAkamaiOptimizerSettings extends ConfigFormBase implements TrustedCallbackInterface {

  /**
   * Config settings.
   *
   * @var string
   */
  const SETTINGS = 'purge_akamai_optimizer.settings';

  /**
   * ReduceTags service.
   *
   * @var Drupal\purge_akamai_optimizer\Services\ReduceTags
   */
  protected $tagsReducer;

  /**
   * Constructs a PurgeAkamaiOptimizerSettings object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\purge_akamai_optimizer\Services\ReduceTags $tagsReducer
   *   The ReduceTags service.
   */
  public function __construct(ConfigFactoryInterface $config_factory, ReduceTags $tagsReducer) {
    $this->setConfigFactory($config_factory);
    $this->tagsReducer = $tagsReducer;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('purge_akamai_optimizer.reduce_tags')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'purge_akamai_optimizer_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::SETTINGS);

    $form['site_identifier_tag'] = [
      '#type' => 'item',
      '#title' => $this->t('Site identifier tag'),
      '#description' => $this->t('A unique identifier generated for site which is added on each page for purging.'),
      '#markup' => _akamai_site_identifier_tag() ?: $this->t('No identifier can be generated'),
    ];

    $form['file_identifier_tag'] = [
      '#type' => 'item',
      '#title' => $this->t('File identifier tag'),
      '#description' => $this->t('A unique file identifier generated for files, such as css, svg, ttf, png, etc.'),
      '#markup' => _akamai_file_identifier_tag() ?: $this->t('No identifier can be generated'),
    ];

    $form['priority_replacement'] = [
      '#type' => 'details',
      '#title' => $this->t('Priority Tags setup status'),
      '#collapsed' => TRUE,
    ];

    $form['priority_replacement']['db_state'] = [
      '#type' => 'item',
      '#title' => $this->t('Replacement table state'),
      '#description' => $this->t('Reflects the state of tags reduction logic. If it is not optimal then on next cron run priority tags will be generated'),
      '#markup' => $this->tagsReducer->shouldGeneratePriorityTags() ? $this->t('Not optimal') : $this->t('Optimal'),
    ];

    $priority_tags = $this->tagsReducer->getPriorityReplacementTags();
    $form['priority_replacement']['priority_tags'] = [
      '#type' => 'item',
      '#title' => $this->t('Priority tags'),
      '#description' => $this->t('List of computed priority tags, these tags are considered for replacement before cache tag prefixes setting.'),
      '#markup' => !empty($priority_tags) ? implode(PHP_EOL, $priority_tags) : $this->t('No priority list is generated yet.'),
    ];

    $last_executed_timestamp = $this->tagsReducer->getLastRunTimestamp();
    $form['priority_replacement']['last_executed'] = [
      '#type' => 'item',
      '#title' => $this->t('Last executed'),
      '#description' => $this->t('Indicates the last time optimisation process was executed.'),
      '#markup' => $last_executed_timestamp ? date('d/m/y h:i:s', $last_executed_timestamp) : $this->t('Not executed yet.'),
    ];

    $form['replace_tags_prefix'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Cache Tag Prefixes'),
      '#default_value' => $config->get('replace_tags_prefix'),
      '#description' => $this->t('List of tag prefixes to be replaced with an identifier. One per line.'),
      '#pre_render' => [[$this, 'implodeElement']],
    ];

    $form['expiry'] = [
      '#type' => 'number',
      '#title' => $this->t('Expiry time for replacement identifier'),
      '#default_value' => $config->get('expiry') ?? 3,
      '#description' => $this->t('Maximum time a replacement identifier record will be stored in database.'),
      '#field_suffix' => $this->t('days'),
    ];

    $form['replace_threshold'] = [
      '#type' => 'number',
      '#title' => $this->t('Replace thresold'),
      '#default_value' => $config->get('replace_threshold') ?? 500,
      '#description' => $this->t('During invalidation, If number of replacements generated is greater than threshold, then site identifier will be used for purging akamai.'),
    ];

    $form['debug'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Debug Settings'),
    ];

    $form['debug']['disable_hashing'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable hashing of cache tags.'),
      '#default_value' => $config->get('disable_hashing'),
      '#description' => $this->t('Disables hashing of cache tags, so that all tags will render.'),
    ];

    $form['debug']['monitor_performance'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Monitor performace for hashing and tags reduction.'),
      '#default_value' => $config->get('monitor_performance'),
      '#description' => $this->t('Logs the time used for reduction and hashing of tags.'),
    ];

    $form['debug']['disable_akamai_caching'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Disable akamai caching.'),
      '#default_value' => $config->get('disable_akamai_caching'),
      '#description' => $this->t('Disables the akamai caching for this site by setting edge-control header to no-cache, no-store.'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $replace_prefixes = trim($values['replace_tags_prefix']);
    $replace_prefixes = !empty($replace_prefixes) ? array_map('trim', explode(PHP_EOL, $replace_prefixes)) : [];

    // Save the configuration.
    $this->configFactory->getEditable(static::SETTINGS)
      ->set('replace_tags_prefix', $replace_prefixes)
      ->set('expiry', $values['expiry'])
      ->set('replace_threshold', $values['replace_threshold'])
      ->set('disable_hashing', $values['disable_hashing'])
      ->set('monitor_performance', $values['monitor_performance'])
      ->set('disable_akamai_caching', $values['disable_akamai_caching'])
      ->save();

    // Clear the rendered cache so that changes can reflect immediately.
    Cache::invalidateTags(['rendered']);

    parent::submitForm($form, $form_state);
  }

  /**
   * Implodes an array using PHP_EOL.
   *
   * @param array $element
   *   The element array.
   *
   * @return array
   *   Returns an element after manipulation.
   */
  public function implodeElement(array $element) {
    $element['#value'] = !empty($element['#value']) ? implode(PHP_EOL, $element['#value']) : '';
    return $element;
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['implodeElement'];
  }

}
