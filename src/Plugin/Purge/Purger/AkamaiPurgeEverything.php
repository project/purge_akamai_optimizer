<?php

namespace Drupal\purge_akamai_optimizer\Plugin\Purge\Purger;

use Drupal\akamai\AkamaiClientFactory;
use Drupal\purge\Plugin\Purge\Purger\PurgerBase;
use Drupal\Core\Config\ConfigFactoryInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\purge\Plugin\Purge\Invalidation\InvalidationInterface;

/**
 * Akamai Purge Everything.
 *
 * @PurgePurger(
 *   id = "akamai_everything",
 *   label = @Translation("Akamai Purge Everything"),
 *   description = @Translation("Provides a Purge service for purging all pages at akamai."),
 *   types = {"everything"},
 * )
 */
class AkamaiPurgeEverything extends PurgerBase {


  /**
   * Web services client for Akamai API.
   *
   * @var \Drupal\akamai\AkamaiClient
   */
  protected $client;

  /**
   * Akamai client config.
   *
   * @var \Drupal\Core\Config
   */
  protected $akamaiClientConfig;

  /**
   * A logger instance.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $logger;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory'),
      $container->get('akamai.client.factory'),
      $container->get('logger.channel.purge_akamai_optimizer')
    );
  }

  /**
   * Constructs a \Drupal\Component\Plugin\AkamaiPurgeEverything.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config
   *   The factory for configuration objects.
   * @param \Drupal\akamai\AkamaiClientFactory $akamai_client_factory
   *   The akamai client factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger interface.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ConfigFactoryInterface $config, AkamaiClientFactory $akamai_client_factory, LoggerInterface $logger) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->client = $akamai_client_factory->get();
    $this->akamaiClientConfig = $config->get('akamai.settings');
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function getTimeHint() {
    $timeout = (float) $this->akamaiClientConfig->get('timeout');
    // The max value for getTimeHint is 10.00.
    if ($timeout > 10) {
      return 10;
    }
    elseif ($timeout > 0) {
      return $timeout;
    }
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function invalidate(array $invalidations) {
    $tags_to_clear = [];
    $site_identifier_tag = _akamai_site_identifier_tag();
    if ($site_identifier_tag) {
      $tags_to_clear[] = $site_identifier_tag;
    }

    // Get the file Identifier Tag.
    $file_identifier_tag = _akamai_file_identifier_tag();
    if ($file_identifier_tag) {
      $tags_to_clear[] = $file_identifier_tag;
    }

    // By default set the state to be failed.
    $invalidation_state = InvalidationInterface::FAILED;

    if (!empty($tags_to_clear)) {
      // Set invalidation type to tag.
      $this->client->setType('tag');

      // Purge tags.
      $invalidation_state = InvalidationInterface::SUCCEEDED;
      $result = $this->client->purgeTags($tags_to_clear);
      if (!$result) {
        $invalidation_state = InvalidationInterface::FAILED;
      }
    }

    // Set Invalidation status.
    foreach ($invalidations as $invalidation) {
      $invalidation->setState($invalidation_state);
    }
  }

  /**
   * Use a static value for purge queuer performance.
   *
   * @see parent::hasRunTimeMeasurement()
   */
  public function hasRuntimeMeasurement() {
    return FALSE;
  }

}
