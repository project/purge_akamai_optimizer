<?php

namespace Drupal\purge_akamai_optimizer\Services;

use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\purge\Plugin\Purge\Invalidation\Exception\TypeUnsupportedException;
use Drupal\purge\Plugin\Purge\Invalidation\InvalidationsServiceInterface;
use Drupal\purge\Plugin\Purge\Queue\QueueServiceInterface;
use Drupal\purge\Plugin\Purge\Queuer\QueuersServiceInterface;

/**
 * Queues identifiers tags for invalidation.
 */
class QueueTags {

  /**
   * The 'purge.invalidation.factory' service.
   *
   * @var \Drupal\purge\Plugin\Purge\Invalidation\InvalidationsServiceInterface
   */
  protected $purgeInvalidationFactory;

  /**
   * The 'purge.queue' service.
   *
   * @var \Drupal\purge\Plugin\Purge\Queue\QueueServiceInterface
   */
  protected $purgeQueue;

  /**
   * The 'purge.queuers' service.
   *
   * @var \Drupal\purge\Plugin\Purge\Queuer\QueuersServiceInterface
   */
  protected $purgeQueuers;

  /**
   * Constructs a QueueTags object.
   *
   * @param Drupal\purge\Plugin\Purge\Invalidation\InvalidationsServiceInterface $purgeInvalidationFactory
   *   The 'purge.invalidation.factory' service.
   * @param \Drupal\purge\Plugin\Purge\Queue\QueueServiceInterface $purgeQueue
   *   The 'purge.queue' service.
   * @param \Drupal\purge\Plugin\Purge\Queuer\QueuersServiceInterface $purgeQueuers
   *   The 'purge.queuers' service.
   */
  public function __construct(InvalidationsServiceInterface $purgeInvalidationFactory, QueueServiceInterface $purgeQueue, QueuersServiceInterface $purgeQueuers) {
    $this->purgeInvalidationFactory = $purgeInvalidationFactory;
    $this->purgeQueue = $purgeQueue;
    $this->purgeQueuers = $purgeQueuers;
  }

  /**
   * Adds the tags in the queue.
   *
   * @param array $tags
   *   An array of tags which needs to be added in purge queue.
   */
  public function queueTags(array $tags) {
    $queuer = $this->purgeQueuers->get('coretags');
    if ($queuer !== FALSE) {
      $invalidations = [];
      foreach ($tags as $tag) {
        try {
          $invalidations[] = $this->purgeInvalidationFactory->get('tag', $tag);
        }
        catch (TypeUnsupportedException $e) {
          // When there's no purger enabled for it, don't bother queuing tags.
          return;
        }
        catch (PluginNotFoundException $e) {
          // When Drupal uninstalls Purge, rebuilds plugin caches it might
          // run into the condition where the tag plugin isn't available. In
          // these scenarios we want the queuer to silently fail.
          return;
        }
      }

      // Add the invalidation in the quqeue.
      if (count($invalidations)) {
        $this->purgeQueue->add($queuer, $invalidations);
      }
    }
  }

}
