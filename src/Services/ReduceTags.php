<?php

namespace Drupal\purge_akamai_optimizer\Services;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\purge_akamai_optimizer\Form\PurgeAkamaiOptimizerSettings;
use Drupal\Core\State\StateInterface;

/**
 * A Class for ReduceTags.
 *
 * A service containg various useful methods for reducing cache tags.
 */
class ReduceTags {

  /**
   * Maximum number of akamai tags to be set.
   */
  const MAX_AKAMAI_TAGS = 127;

  /**
   * DELIMITER for reducing the tags.
   *
   * @var string
   */
  const DELIMITER = ',';

  /**
   * Name of the state where priority settings are stored.
   */
  const PRIORITY_SETTINGS = 'pao_priority_replacement';

  /**
   * Name of the table where replacement tags are stored.
   */
  const REPLACEMENT_TABLE = 'purge_optimizer_removed_tags';

  /**
   * The config factory object.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The db connection object.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $dbConnection;

  /**
   * The state object.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected $state;

  /**
   * Constructs a ReduceTags object.
   *
   * @param Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory object.
   * @param Drupal\Core\Database\Connection $db_connection
   *   The config factory object.
   * @param Drupal\Core\State\StateInterface $state
   *   The state object.
   */
  public function __construct(ConfigFactoryInterface $config_factory, Connection $db_connection, StateInterface $state) {
    $this->configFactory = $config_factory;
    $this->dbConnection = $db_connection;
    $this->state = $state;
  }

  /**
   * Returns an array of cache tags prefixes which might be remove from a page.
   *
   * @return array
   *   List of cache tags pattern to remove in order of decreasing priority.
   */
  public function reduceTagsPrefixes() {
    $priority_replacement = $this->getPriorityReplacementTags();
    $tags = $this->configFactory->getEditable(PurgeAkamaiOptimizerSettings::SETTINGS)->get('replace_tags_prefix');
    return array_merge($priority_replacement, $tags);
  }

  /**
   * Returns an array of tags after removing the extra tags.
   *
   * @param array $tags
   *   The tags array from which the tags to be removed.
   * @param int $limit
   *   Maximum number of tags to return.
   *
   * @return array
   *   The tags array after removing extra tags.
   */
  public function removeExtraTags(array $tags, $limit = self::MAX_AKAMAI_TAGS) {
    $tags_to_keep = $tags;
    $reduce_prefixes = $this->reduceTagsPrefixes();
    $tags_removed = [];
    if (count($tags_to_keep) > $limit) {
      // Reduce the tags here.
      foreach ($reduce_prefixes as $prefix) {
        foreach ($tags_to_keep as $index => $tag) {
          if (substr($tag, 0, strlen($prefix)) === $prefix) {
            $tags_removed[] = $tag;
            unset($tags_to_keep[$index]);
          }
          if (count($tags_to_keep) < $limit) {
            // We are in the acceptable limit now hence exit from loops.
            break 2;
          }
        }
      }
    }
    $return_tags = [
      'tags_keep' => $tags_to_keep,
      'tags_removed' => $tags_removed,
    ];

    return $return_tags;
  }

  /**
   * Store the tags in db and return the identifier.
   *
   * @param string $identifier
   *   The unique identifier.
   * @param array $tags_removed
   *   The removed tags.
   */
  public function storeRemovedTags(string $identifier, array $tags_removed) {
    $expiry = $this->getDefaultExpiry();
    $expiry_days = strtotime("+" . $expiry . "days");
    // Store the identifier and tags in the database.
    $this->dbConnection->merge(self::REPLACEMENT_TABLE)
      ->key('identifier', $identifier)
      ->fields([
        'identifier' => $identifier,
        'tags' => implode(self::DELIMITER, $tags_removed),
        'expires' => $expiry_days,
      ])
      ->execute();
  }

  /**
   * Gets the default expiry time for identifier records.
   *
   * @return int
   *   Number of days after which each identifier record will expire.
   */
  public function getDefaultExpiry() {
    $expiry = $this->configFactory->getEditable(PurgeAkamaiOptimizerSettings::SETTINGS)->get('expiry') ?? 3;
    return $expiry;
  }

  /**
   * Returns a list of identifiers containing one of the mentioned tag.
   *
   * @param array $tags
   *   List of tags for which identifier is needed.
   *
   * @return array
   *   List of identifiers.
   */
  public function getIdentifiers(array $tags) {
    $identifiers = [];

    // Get the identifiers here.
    $query = $this->dbConnection->select(self::REPLACEMENT_TABLE, 'rt');
    $orGroup = $query->orConditionGroup();
    foreach ($tags as $tag) {
      // Match the tag at the start.
      $orGroup->condition('rt.tags', "$tag" . self::DELIMITER . "%", 'Like');

      // Match the tag in the middle.
      $orGroup->condition('rt.tags', "%" . self::DELIMITER . "$tag" . self::DELIMITER . "%", 'Like');

      // Match the tag at the end.
      $orGroup->condition('rt.tags', "%" . self::DELIMITER . "$tag", 'Like');
    }
    $query->condition($orGroup);
    $query->fields('rt', ['identifier']);
    $identifiers = $query->execute()->fetchCol();

    return $identifiers;
  }

  /**
   * Removes all the expired identifier records.
   */
  public function removeOldIdentifiers() {
    $this->dbConnection->delete(self::REPLACEMENT_TABLE)
      ->condition('expires', strtotime('now'), '<')
      ->execute();
  }

  /**
   * Generates a priority list of tags from existing records and save it.
   *
   * @param bool $store
   *   If true the generated tags will be stored as well.
   *
   * @return array
   *   Tags which are analysed as a priority for replacement.
   */
  public function generatePriorityListTags(bool $store = TRUE) {
    // Get all the tags in all records.
    $query = $this->dbConnection->select(self::REPLACEMENT_TABLE, 'rt');
    $query->fields('rt', ['tags']);
    $rows = $query->execute()->fetchCol();

    // Count number of tags in each row & combine all tags in a single array.
    $counts = [];
    $tags = [];
    foreach ($rows as $row) {
      $tags_per_record = explode(',', $row);
      $counts[] = count($tags_per_record);
      $tags = array_unique(array_merge($tags, $tags_per_record));
    }

    // Get an average number of tags per row.
    $average_num_tags = (int) (array_sum($counts) / count($counts));

    // Count how many times each tag appears in identifer record.
    $final_tags = array_count_values($tags);

    // Sort data in ascending order while preserving the keys.
    asort($final_tags);

    // Get the tags which are to be prioritized.
    $len = count($final_tags);
    $req = $average_num_tags;
    $new_list = array_reverse(array_keys(array_slice($final_tags, $len - $req)));

    if ($store) {
      $this->savePriorityReplacementTags($new_list);
    }

    return $new_list;
  }

  /**
   * Save the priority list data.
   *
   * @param array $tags
   *   List of tags to be stored in priority list.
   */
  public function savePriorityReplacementTags(array $tags) {
    $this->state->set(self::PRIORITY_SETTINGS, [
      'tags' => $tags,
      'last_updated_time' => time(),
    ]);
  }

  /**
   * Get the priority list data.
   *
   * @return array
   *   An array of computed priority tags.
   */
  public function getPriorityReplacementTags() {
    $tags = [];
    $priority_replacement = $this->state->get(self::PRIORITY_SETTINGS);
    if (!empty($priority_replacement['tags'])) {
      $tags = $priority_replacement['tags'];
    }
    return $tags;
  }

  /**
   * Get the last time when the priority list generation process executed.
   *
   * @return int
   *   Zero for no execution otherwise timestamp of last execution.
   */
  public function getLastRunTimestamp() {
    // Zero indicates the process has not excuted yet.
    $time = 0;
    $priority_replacement = $this->state->get(self::PRIORITY_SETTINGS);
    if (!empty($priority_replacement['last_updated_time'])) {
      $time = $priority_replacement['last_updated_time'];
    }
    return $time;
  }

  /**
   * Check whether the criteria for generating prioity list is met.
   *
   * @return bool
   *   If this is false it might imply that system is already in optimal state.
   */
  public function shouldGeneratePriorityTags() {
    $flag = FALSE;
    $last_run = $this->getLastRunTimestamp();
    $expiry = (int) ($this->getDefaultExpiry() / 2);

    // Check if expiry_time/2 days has passed since last execution.
    if ($last_run < strtotime('-' . $expiry . ' days')) {
      // Check if records are going over half the number of nodes.
      $node_count = $this->dbConnection->select('node')->countQuery()->execute()->fetchField();
      $identifier_count = $this->dbConnection->select(self::REPLACEMENT_TABLE)->countQuery()->execute()->fetchField();
      if ($identifier_count > $node_count / 2) {
        $flag = TRUE;
      }
    }

    return $flag;
  }

}
